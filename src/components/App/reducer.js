function reducer(state, action) {
    let newState;

    switch (action.type) {
        case 'FORM_UPDATE':
            newState = {...state, form: {...state.form, [action.name]: action.value}};
            break;

        case 'LOGGED_IN':
            newState = {...state, isLoggedIn: action.status};
            break;

        default:
            newState = state;
    }

    return newState;
}

export default reducer;