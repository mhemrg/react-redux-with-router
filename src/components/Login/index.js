import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

class Login extends Component {
    handleSubmit = e => {
        e.preventDefault();

        if(this.props.username === 'mhe') {
            this.props.dispatch({
                type: 'LOGGED_IN',
                status: true
            });
        }
        else {
            alert('The username is wrong!')
        }
    }

    handleUpdate = e => {
        const { name, value } = e.target;

        this.props.dispatch({
            name,
            value,
            type: 'FORM_UPDATE',
        });
    }

    render() {
        const warning = {backgroundColor: 'orange', display: 'inline-block', padding: 10};

        return this.props.isLoggedIn ?
            <Redirect to="dashboard" />:
            <div>
                <h1>LoginPage</h1>

                <p style={warning}>You are not logged in!</p>

                <form onSubmit={this.handleSubmit}>
                    <label>username:</label>
                    <input type="text" name="username" onChange={this.handleUpdate} />

                    <div><button>Login -></button></div>
                </form>
            </div>
    }
}

function mapStateToProps(state) {
    return {
        username: state.form.username,
        isLoggedIn: state.isLoggedIn
    }
}

export default connect(mapStateToProps)(Login);