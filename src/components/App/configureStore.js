import { createStore } from 'redux';
import reducer from './reducer';

function configureStore() {
    const initialState = {
        form: {},
        isLoggedIn: false
    };

    return createStore(reducer, initialState);
}

export default configureStore;