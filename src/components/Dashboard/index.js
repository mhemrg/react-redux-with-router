import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Route, Redirect, Link } from 'react-router-dom';

class Dashboard extends Component {
    signout = e => {
        this.props.dispatch({
            type: 'LOGGED_IN',
            status: false
        });
    }

    render() {
        return ! this.props.isLoggedIn ?
            <Redirect to="/login" />:
            <div>
                <div style={{backgroundColor: 'lightblue', padding: 15}}>
                    <h1>Welcome to DashboardPage :)</h1>
                </div>
                <br />
                <button onClick={this.signout}>Sign out -></button>

                <h2>Sections</h2>
                <ul>
                    <li><Link to="/dashboard/posts">Posts</Link></li>
                    <li><Link to="/dashboard/settings">Settings</Link></li>
                </ul>

                <section style={{margin: 20, border: '1px solid orange', padding: 10}}>
                    <Route path="/dashboard/posts" render={match => <h3>You are in: Posts ...</h3>} />
                    <Route path="/dashboard/settings" render={match => <h3>You are in: Settings ...</h3>} />
                </section>
            </div>
    }
}

function mapStateToProps(state) {
    return {
        isLoggedIn: state.isLoggedIn
    }
}

export default connect(mapStateToProps)(Dashboard);